#include <iostream>
using namespace std;

int main() 
{ 
	setlocale(LC_ALL, "Russian");
    cout << "Введите четырёхзначное число" << endl;
	int a;
	cin >>a;
	if (cin.fail()) {cout <<"Введено некорректное число"<<endl; system("pause"); return 0;} else {
	if (a>=1000 && a<=9999) 
	{
	    if ((a/1000 + (a/100)%10) == (a%10 + (a%100)/10)) 
		{
		    cout <<"Число "<<a<<" счастливое"<<endl;
		}
	    else 
		{
		    cout <<"Число "<<a<<" не счастливое"<<endl;
		}
	} 
	else
	{
		cout <<"Введено некорректное число"<<endl;
	}
	}
    system("pause");
    return 0; 
}